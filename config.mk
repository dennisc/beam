# Version, do not change yourself
VERSION = 0.1.0

# Customize for your system

# paths
PREFIX = /usr/local

# flags
CC ?= cc
CFLAGS = -Wall
LDFLAGS = -lm
