#include <stdbool.h>

bool substr_eq(char* c1, int idx1, char* c2, int idx2, int len);
bool is_empty(char* c);
bool contains(char* s, char c);
int trim(char* c);
