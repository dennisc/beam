include config.mk
.PHONY: all beam install test

SRC = beam.c util.c
OBJ = ${SRC:.c=.o}

all: beam

beam: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS} 

install: all
	@echo Installing to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f beam ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/beam

test: beam
	@./beam example.beam
