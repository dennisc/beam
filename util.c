#include <stdbool.h>
#include <string.h>
#include <ctype.h>

// Given two pointers to character arrays, the starting index to look at for each array, and the number of characters to compare, return true if the substrings are identical and false otherwise.
bool substr_eq(char* c1, int idx1, char* c2, int idx2, int len) {
	if (strlen(c1) < idx1 + len || strlen(c2) < idx2 + len) {
		return false;
	}
	for (int i = 0; i < len; i++) {
		if (c1[idx1+1] != c2[idx2+1]) {
			return false;
		}
	}
	return true;
}

// Tests whether a line is all whitespace or not
bool is_empty(char* c) {
	for (int i = 0; i < strlen(c); i++) {
		if (!isspace((unsigned char) *c+i)) {
			return false;
		}
	}
	return true;
}

bool contains(char* s, char c) {
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] == c) {
			return true;
		}
	}
	return false;
}

int trim(char* c) {
	for (int i = 0; i < strlen(c); i++) {
		if (!isspace((unsigned char) *c+i)) {
			return i;
		}
	}
	return strlen(c);
}
