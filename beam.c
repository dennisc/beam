#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sysexits.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "util.h"

struct Slide {
	char* title; // Possibly null pointer, this substitutes for Option<String> in Rust
	char* image; // Can be null pointer again
	char* text; // Cannot be null pointer
};

static const struct Slide EmptySlide = { NULL };

char* slide_to_tex(struct Slide s) {
	char* tex = malloc(BUFSIZ);
	char* current_env = malloc(BUFSIZ);
	char prev_leader = '\0';

	#define format_env(env_name, title, line_prefix) {\
		current_env[strlen(current_env)-1] = '\0';\
		char* found;\
		if (title) {\
			found = strsep(&current_env, "\n");\
			strcat(tex, "\\begin{");\
			strcat(tex, env_name);\
			strcat(tex, "}{");\
			strcat(tex, found + trim(found));\
			strcat(tex, "}");\
		} else {\
			strcat(tex, "\\begin{");\
			strcat(tex, env_name);\
			strcat(tex, "}");\
		}\
		while ( (found = strsep(&current_env, "\n")) != NULL ) {\
			strcat(tex, "\n");\
			strcat(tex, "\t");\
			strcat(tex, line_prefix);\
			strcat(tex, found + trim(found));\
			strcat(tex, "\n");\
		}\
		strcat(tex, "\\end{");\
		strcat(tex, env_name);\
		strcat(tex, "}\n");\
	}

	#define push_env() {\
		switch (prev_leader) {\
			case '-':\
				format_env("itemize", false, "\\item ")\
				break;\
			case '>':\
				format_env("block", true, "")\
				break;\
			case '<':\
				format_env("exampleblock", true, "")\
				break;\
			case '!':\
				format_env("alertblock", true, "")\
				break;\
			default:\
				break;\
		}\
		free(current_env);\
		current_env = malloc(BUFSIZ);\
		prev_leader = '\0';\
	}

	if (s.image != NULL) {
		s.image[strlen(s.image)-1] = '\0';
		strcat(tex, "{\\setbeamertemplate{background}{\\includegraphics[width=\\paperwidth, height=\\paperheight]{../");
		strcat(tex, s.image);
		strcat(tex, "}}\n");
	}

	if (s.title != NULL) {
		s.title[strlen(s.title)-1] = '\0';
		strcat(tex, "\\begin{frame}{");
		strcat(tex, s.title + trim(s.title));
		strcat(tex, "}\n");
	} else {
		strcat(tex, "\\begin{frame}\n");
	}

	char* found;
	while ( (found = strsep(&s.text, "\n")) != NULL ) {
		if (!is_empty(found)) {
			char leader = found[0];
			if (leader != prev_leader) {
				push_env();
			}
			if (contains("-><!", leader)) {
				strcat(current_env, found+1);
				strcat(current_env, "\n");
				prev_leader = leader;
			} else {
				strcat(tex, found);
				strcat(tex, "\n");
				strcat(tex, "\n");
				prev_leader = '\0';
			}
		} else {
			push_env();
		}
	}
	
	if (tex[strlen(tex)-1] == '\n' && tex[strlen(tex)-2] == '\n') {
		tex[strlen(tex)-1] = '\0';
	}
	strcat(tex, "\\end{frame}");

	if (s.image != NULL) {
		strcat(tex, "\n}\n\n");
	} else {
		strcat(tex, "\n\n");
	}

	return tex;
}

bool slide_is_empty(struct Slide s) {
	return s.title == NULL && s.image == NULL && (s.text == NULL || is_empty(s.text));
}

void compile_presentation(char* beam_name, char* tex_name, char* pdf_name) {
	char* preamble = malloc(BUFSIZ);
	char* contents = malloc(BUFSIZ);
	struct Slide CurrentSlide = EmptySlide;
	CurrentSlide.text = malloc(BUFSIZ);
	
	#define push_slide() {\
		if (!slide_is_empty(CurrentSlide)) {\
			strcat(contents, slide_to_tex(CurrentSlide));\
			free(CurrentSlide.text);\
			CurrentSlide = EmptySlide;\
			CurrentSlide.text = malloc(BUFSIZ);\
		}\
	}

	FILE* fpr = fopen(beam_name, "r");
	if (fpr == NULL) {
		printf("File %s could not be found", beam_name);
		exit(EX_NOINPUT);
	}
	char buf[BUFSIZ];
	while(fgets(buf, sizeof(buf), fpr)) {
		if (strlen(buf) == 0) {
			push_slide();
			continue;
		}
		switch (buf[0]) {
			case '#':
				push_slide();
				buf[strlen(buf)-1] = '\0';
				strcat(contents, "\\section{");
				strcat(contents, buf+1+trim(buf+1));
				strcat(contents, "}\n\n");
				break;
			case '^':
				push_slide();
				strcat(preamble, buf+1+trim(buf+1));
				break;
			case '~':
				push_slide();
				CurrentSlide.title = malloc(strlen(buf));
				strcpy(CurrentSlide.title, buf+1+trim(buf+1));
				break;
			case '@':
				CurrentSlide.image = malloc(strlen(buf));
				strcpy(CurrentSlide.image, buf+1+trim(buf+1));
				break;
			default:
				strcat(CurrentSlide.text, buf+trim(buf));
				break;
		}
	}
	fclose(fpr);
	
	DIR* dir = opendir("build");
	if (!dir) {
		mkdir("build", 0700);
	}
	chdir("build");
	FILE* fpw = fopen(tex_name, "w");
	fprintf(fpw, "%s\n\\begin{document}\n%s\\end{document}", preamble, contents);
	fclose(fpw);
	if (fork() == 0) {
		char* args[] = {"latexmk", "-pdf", "-f", NULL};
		execvp("latexmk", args);
	}
	while (wait(NULL) > 0);
	char* new_name = malloc(3+strlen(pdf_name)+1);
	strcpy(new_name, "../");
	strcat(new_name, pdf_name);
	int status = rename(pdf_name, new_name);
	if (status != 0) {
		fprintf(stderr, "Renaming %s failed", pdf_name);
	}
	chdir("..");
	
	// Cleanup (call destructors)
	free(preamble);
	free(contents);
}

int main(int argc, char* argv[]) {
	for (int i = 1; i < argc; i++) {
		char* raw_name; // beam_name
		char* beam_name; // beam_name.beam
		
		if (strlen(argv[i]) > 5 && substr_eq(argv[i], strlen(argv[i])-5, ".beam", 0, 5)) {
			beam_name = argv[i]; // Pass on ownership, stop calling argv[i] now and exclusively use `beam_name`
			raw_name = malloc(strlen(beam_name) - 5 + 1); // We leave one bit for the null terminator '\0'
			for (int j = 0; j < strlen(beam_name)-5; j++) {
				raw_name[j] = beam_name[j];
			}
			raw_name[strlen(raw_name)] = '\0';
		} else {
			raw_name = argv[i];
			beam_name = malloc(strlen(raw_name) + 5 + 1);
			for (int j = 0; j < strlen(raw_name); j++) {
				beam_name[j] = raw_name[j];
			}
			strcpy(beam_name + strlen(raw_name), ".beam");
		}
		
		char* tex_name = malloc(strlen(beam_name) + 4 + 1); // beam_name.tex
		char* pdf_name = malloc(strlen(beam_name) + 4 + 1); // beam_name.pdf
		
		for (int j = 0; j < strlen(raw_name); j++) {
			tex_name[j] = raw_name[j];
			pdf_name[j] = raw_name[j];
		}

		strcpy(tex_name + strlen(raw_name), ".tex");
		strcpy(pdf_name + strlen(raw_name), ".pdf");

		compile_presentation(beam_name, tex_name, pdf_name);

		free(tex_name);
		free(pdf_name);
	}
	return 0;
}
